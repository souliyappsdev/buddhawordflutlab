import 'dart:isolate';
import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:advance_pdf_viewer/advance_pdf_viewer.dart';
import 'package:flutter_downloader/flutter_downloader.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';

const kAndroidUserAgent =
    'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N; Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Mobile Safari/537.36';

Future main() async {
  WidgetsFlutterBinding.ensureInitialized();

  await FlutterDownloader.initialize();
  WidgetsFlutterBinding.ensureInitialized();
  await FlutterDownloader.initialize(
      debug: true // optional: set false to disable printing logs to console
      );
  runApp(Book2Lao());
}

class MyChromeSafariBrowser extends ChromeSafariBrowser {
  @override
  void onOpened() {
    print("ChromeSafari browser opened");
  }

  @override
  void onCompletedInitialLoad() {
    print("ChromeSafari browser initial load completed");
  }

  @override
  void onClosed() {
    print("ChromeSafari browser closed");
  }
}

class Book2Lao extends StatefulWidget {
  final ChromeSafariBrowser browser = new MyChromeSafariBrowser();

  @override
  _Book2LaoState createState() => _Book2LaoState();
}

class _Book2LaoState extends State<Book2Lao> {
  bool _isLoading = true;
  late PDFDocument document;

  ReceivePort _port = ReceivePort();

  @override
  void initState() {
    loadDocument();
    super.initState();
    widget.browser.addMenuItem(new ChromeSafariBrowserMenuItem(
        id: 1,
        label: 'Custom item menu 1',
        action: (url, title) {
          print('Custom item menu 1 clicked!');
        }));
    IsolateNameServer.registerPortWithName(
        _port.sendPort, 'downloader_send_port');
    _port.listen((dynamic data) {
      // ignore: unused_local_variable
      String id = data[0];
      DownloadTaskStatus status = data[1];
      // ignore: unused_local_variable
      int progress = data[2];
      if (status == DownloadTaskStatus.complete) {
        print('Download Completed!');
      }
      setState(() {});
    });
  }

  @override
  void dispose() {
    IsolateNameServer.removePortNameMapping('downloader_send_port');
    super.dispose();
  }

  loadDocument() async {
    document = await PDFDocument.fromURL(
      "https://files.fm/down.php?i=kxa8qej9g",
    );

    setState(() => _isLoading = false);
  }

  changePDF(value) async {
    setState(() => _isLoading = true);
    if (value == 1) {
      document = await PDFDocument.fromURL(
        "https://files.fm/down.php?i=kxa8qej9g",
      );
    } else if (value == 2) {
      document = await PDFDocument.fromURL(
        "https://files.fm/down.php?i=kxa8qej9g",
      );
    } else {
      document = await PDFDocument.fromURL(
        "https://files.fm/down.php?i=kxa8qej9g",
      );
    }
    setState(() => _isLoading = false);
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        // Define the default brightness and colors.
        brightness: Brightness.dark,
        canvasColor: Colors.brown[400],

        // Define the default font family.
        fontFamily: 'Georgia',

        // Define the default `TextTheme`. Use this to specify the default
        // text styling for headlines, titles, bodies of text, and more.
        textTheme: const TextTheme(
          // headline1: TextStyle(fontSize: 50.0, fontWeight: FontWeight.bold),
          // headline6: TextStyle(fontSize: 30.0, fontStyle: FontStyle.italic),
          bodyText2: TextStyle(fontSize: 30.0, fontFamily: 'Georgia'),
        ),
      ),
      // theme: ThemeData(
      //   canvasColor: Colors.brown[400],
      // ),
      home: Scaffold(
        resizeToAvoidBottomInset: true,
        appBar: AppBar(
          iconTheme: IconThemeData(color: Colors.brown[400]),
          backgroundColor: Colors.brown,
          elevation: 0,
          // centerTitle: true,
          actions: [
            Padding(
              padding: EdgeInsets.only(right: 20),
              child: IconButton(
                color: Colors.white,
                icon: Icon(Icons.arrow_back),
                iconSize: 32.0,
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(right: 100),
              child: Text(
                "📚(ຄູ່ມືໂສດາບັນ)",
                style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: 25,
                  height: 1.8,
                ),
              ),
            ),
            Padding(
                padding: EdgeInsets.only(left: 0),
                child: IconButton(
                  color: Colors.white,
                  icon: Icon(Icons.file_download),
                  iconSize: 32.0,
                  onPressed: () async {
                    await widget.browser.open(
                      url: Uri.parse(
                          "https://fv9-3.failiem.lv/down.php?i=kxa8qej9g&n=%E0%BA%84%E0%BA%B9%E0%BB%88%E0%BA%A1%E0%BA%B7%E0%BB%82%E0%BA%AA%E0%BA%94%E0%BA%B2%E0%BA%9A%E0%BA%B1%E0%BA%99.pdf&download_checksum=cdb1c707b453f8b9b9ada677395f40ce24f7a646&download_timestamp=1633841155"),
                      options: ChromeSafariBrowserClassOptions(
                        android: AndroidChromeCustomTabsOptions(
                            addDefaultShareMenuItem: false),
                        ios: IOSSafariOptions(barCollapsingEnabled: true),
                      ),
                    );
                  },
                )),
          ],
        ),
        body: Container(
          color: Theme.of(context).disabledColor,
          child: _isLoading
              ? Center(
                  child: CircularProgressIndicator(),
                )
              : PDFViewer(
                  document: document,
                  zoomSteps: 1,
                  // scrollDirection: Axis.vertical,
                ),
        ),
      ),
    );
  }
}
