import 'dart:io';
import 'dart:async';
// import 'package:buddhaword/screens/Books.dart';
import 'package:buddhaword/screens/updatescreen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:flutter/rendering.dart';
import 'package:buddhaword/sidenav_lao/book_1-lao.dart';
import 'package:buddhaword/sidenav_lao/book_2-lao.dart';
import 'package:buddhaword/sidenav_lao/book_3-lao.dart';
import 'package:buddhaword/sidenav_lao/book_4-lao.dart';
import 'package:buddhaword/sidenav_lao/book_5-lao.dart';
import 'package:buddhaword/sidenav_lao/book_6-lao.dart';
import 'package:buddhaword/sidenav_lao/book_7-lao.dart';
import 'package:buddhaword/sidenav_lao/book_8-lao.dart';
import 'package:buddhaword/sidenav_lao/diagram_1-lao.dart';
import 'package:permission_handler/permission_handler.dart';

const kAndroidUserAgent =
    'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N; Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Mobile Safari/537.36';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();

  if (Platform.isAndroid) {
    await AndroidInAppWebViewController.setWebContentsDebuggingEnabled(true);
    WidgetsFlutterBinding.ensureInitialized();
    await Permission.camera.request();
    await Permission.microphone.request();
    await Permission.storage.request();

    var swAvailable = await AndroidWebViewFeature.isFeatureSupported(
        AndroidWebViewFeature.SERVICE_WORKER_BASIC_USAGE);
    var swInterceptAvailable = await AndroidWebViewFeature.isFeatureSupported(
        AndroidWebViewFeature.SERVICE_WORKER_SHOULD_INTERCEPT_REQUEST);

    if (swAvailable && swInterceptAvailable) {
      AndroidServiceWorkerController serviceWorkerController =
          AndroidServiceWorkerController.instance();

      serviceWorkerController.serviceWorkerClient = AndroidServiceWorkerClient(
        shouldInterceptRequest: (request) async {
          print(request);
          return null;
        },
      );
    }
  }

  runApp(SearchScreen());
}

class MyChromeSafariBrowser extends ChromeSafariBrowser {
  @override
  void onOpened() {
    print("ChromeSafari browser opened");
  }

  @override
  void onCompletedInitialLoad() {
    print("ChromeSafari browser initial load completed");
  }

  @override
  void onClosed() {
    print("ChromeSafari browser closed");
  }
}

class SearchScreen extends StatefulWidget {
  // SearchScreen({Key? key}) : super(key: key);
  final ChromeSafariBrowser browser = new MyChromeSafariBrowser();

  @override
  State<SearchScreen> createState() => _SearchScreenState();
}

class _SearchScreenState extends State<SearchScreen> {
  double _progress = 0;

  late InAppWebViewController webView;

  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();

  final GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();

  late InAppWebViewController controller;

  get bottomNavigationBar => null;

  PageController pageController = PageController();

  get tabs => null;

  TextEditingController searchController = TextEditingController();
  int navIndex = 0;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  Future webViewMethod() async {
    print('In Microphone permission method');
    WidgetsFlutterBinding.ensureInitialized();

    Permission.microphone.request();
    Permission.camera.request();
    await WebViewMethodForCamera();
  }

  // ignore: non_constant_identifier_names
  Future WebViewMethodForCamera() async {
    print('In Camera permission method');
    WidgetsFlutterBinding.ensureInitialized();
    Permission.camera.request();
    Permission.microphone.request();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      key: scaffoldKey,
      drawer: Sidenav(navIndex, (int index) {
        setState(() {
          navIndex = index;
        });
      }),
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        actions: <Widget>[
          // Padding(
          //   padding: const EdgeInsets.only(right: 0.0),
          //   child: ElevatedButton.icon(
          //     icon: new Image.asset(
          //       "assets/Splash_Logo.png",
          //       width: 50,
          //       height: 50,
          //     ),
          //     label: Text(
          //       "",
          //       // textAlign: TextAlign.center,
          //     ),
          //     style: ElevatedButton.styleFrom(
          //       textStyle: TextStyle(
          //         fontSize: 2,
          //         fontStyle: FontStyle.normal,
          //       ),
          //       fixedSize: const Size(0, 0),
          //       padding: const EdgeInsets.fromLTRB(5, 5, 0, 5),
          //       primary: Colors.transparent,
          //       shadowColor: Colors.transparent,
          //       shape: const BeveledRectangleBorder(
          //           borderRadius: BorderRadius.all(Radius.circular(0))),
          //     ),
          //     onPressed: () {
          //       print('Pressed');
          //       Navigator.push(
          //         context,
          //         MaterialPageRoute(builder: (context) => SearchScreen()),
          //       );
          //     },
          //   ),
          // ),
          IconButton(
            color: Colors.white,
            icon: const Icon(Icons.arrow_back),
            iconSize: 32.0,
            onPressed: () {
              webView.goBack();
            },
          ),
          IconButton(
            color: Colors.white,
            icon: Icon(Icons.ios_share),
            iconSize: 32.0,
            onPressed: () async {
              await widget.browser.open(
                url: Uri.parse(
                    "https://www.appsheet.com/start/5480d3a7-719a-4436-9de6-8da95283b56e"),
                options: ChromeSafariBrowserClassOptions(
                  android: AndroidChromeCustomTabsOptions(
                    addDefaultShareMenuItem: false,
                  ),
                  ios: IOSSafariOptions(
                    barCollapsingEnabled: true,
                  ),
                ),
              );
            },
          ),
          IconButton(
            color: Colors.white,
            icon: const Icon(Icons.arrow_forward),
            iconSize: 32.0,
            onPressed: () {
              webView.goForward();
            },
          ),
          IconButton(
            color: Colors.white,
            icon: const Icon(Icons.refresh),
            iconSize: 32.0,
            onPressed: () async {
              // showNotification();
              // You can request multiple permissions at once.
              Map<Permission, PermissionStatus> statuses = await [
                Permission.microphone,
                Permission.camera,
                //add more permission to request here.
              ].request();

              if (statuses[Permission.microphone]!.isDenied) {
                //check each permission status after.
                print("Microphone permission is denied.");
              }

              if (statuses[Permission.camera]!.isDenied) {
                //check each permission status after.
                print("Camera permission is denied.");
              }
              // ignore: unnecessary_null_comparison
              if (webView != null) {
                webView.reload();
              }
            },
          ),
          IconButton(
            color: Colors.white,
            icon: Icon(Icons.mode_edit),
            iconSize: 32.0,
            onPressed: () async {
              await widget.browser.open(
                url: Uri.parse(
                    "https://www.appsheet.com/start/7d6156a4-7476-4882-8355-f742d49a01a6"),
                options: ChromeSafariBrowserClassOptions(
                  android: AndroidChromeCustomTabsOptions(
                    addDefaultShareMenuItem: false,
                  ),
                  ios: IOSSafariOptions(
                    barCollapsingEnabled: true,
                  ),
                ),
              );
            },
          ),
          // IconButton(
          //   color: Colors.white,
          //   icon: Icon(Icons.menu_book),
          //   iconSize: 32.0,
          //   onPressed: () {
          //     Navigator.push(
          //       context,
          //       MaterialPageRoute(builder: (context) => Books()),
          //     );
          //   },
          // ),

          IconButton(
            color: Colors.white,
            icon: Icon(Icons.menu_book),
            iconSize: 32.0,
            onPressed: () async {
              await widget.browser.open(
                url: Uri.parse("https://buddhaword.siteoly.com/"),
                options: ChromeSafariBrowserClassOptions(
                  android: AndroidChromeCustomTabsOptions(
                    addDefaultShareMenuItem: false,
                  ),
                  ios: IOSSafariOptions(
                    barCollapsingEnabled: true,
                  ),
                ),
              );
            },
          ),
        ],
      ),
      body: Stack(
        fit: StackFit.passthrough,
        children: <Widget>[
          PageView(
            controller: pageController,
            children: <Widget>[
              Container(
                color: Colors.brown,
              ),
            ],
          ),
          InAppWebView(
            initialUrlRequest: URLRequest(
              url: Uri.parse(
                  "https://www.appsheet.com/start/5480d3a7-719a-4436-9de6-8da95283b56e"),
            ),
            androidOnPermissionRequest: (InAppWebViewController controller,
                String origin, List<String> resources) async {
              return PermissionRequestResponse(
                  resources: resources,
                  action: PermissionRequestResponseAction.GRANT);
            },
            onWebViewCreated: (InAppWebViewController controller) {
              webView = controller;
            },
            onReceivedServerTrustAuthRequest: (controller, challenge) async {
              return ServerTrustAuthResponse(
                  action: ServerTrustAuthResponseAction.PROCEED);
            },
            onProgressChanged:
                (InAppWebViewController controller, int progress) {
              setState(
                () {
                  _progress = progress / 100;
                },
              );
            },
            initialOptions: InAppWebViewGroupOptions(
              ios: IOSInAppWebViewOptions(
                allowsInlineMediaPlayback: true,
                limitsNavigationsToAppBoundDomains:
                    true, // adds Service Worker API on iOS 14.0+
              ),
              crossPlatform: InAppWebViewOptions(
                mediaPlaybackRequiresUserGesture: false,
                useOnDownloadStart: true,
                useShouldOverrideUrlLoading: true,
                supportZoom: true,
              ),
            ),
          ),
          _progress < 1
              ? SizedBox(
                  height: 3,
                  child: LinearProgressIndicator(
                    value: _progress,
                    backgroundColor:
                        // ignore: deprecated_member_use
                        Theme.of(context).accentColor.withOpacity(0.2),
                  ),
                )
              : const SizedBox()
        ],
      ),
    );
  }
}

class Sidenav extends StatelessWidget {
  final Function setIndex;
  final int selectedIndex;

  Sidenav(this.selectedIndex, this.setIndex);

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: [
          Padding(
            padding: EdgeInsets.all(10.0),
            child: Text('Menu',
                style: TextStyle(
                    color: Theme.of(context).bottomAppBarColor, fontSize: 21)),
          ),
          Divider(color: Colors.white),
          _navItem(
            context,
            Icons.search,
            '🔎 ຄົ້ນຫາ',
            suffix: Text(
              '',
              style: TextStyle(fontWeight: FontWeight.w500),
            ),
            onTap: () {
              // _navItemClicked(context, 0);
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => SearchScreen()),
              );
            },
            // selected: selectedIndex == 0,
          ),
          Divider(color: Colors.grey.shade400),
          _navItem(
            context,
            Icons.system_update_alt_sharp,
            '📲 ປັບປຸງລະບົບ',
            suffix: Text(
              '',
              style: TextStyle(fontWeight: FontWeight.w500),
            ),
            onTap: () {
              // _navItemClicked(context, 6);
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => UpdateScreen()),
              );
            },
            // selected: selectedIndex == 6,
          ),
          Divider(color: Colors.grey.shade400),
          Padding(
            padding: EdgeInsets.all(16.0),
            child: Text('📚ປຶ້ມ ຄຳສອນພຣະພຸດທະເຈົ້າ',
                style: TextStyle(
                    color: Theme.of(context).bottomAppBarColor, fontSize: 21)),
          ),
          Divider(color: Colors.white),
          Padding(
            padding: EdgeInsets.all(10.10),
            child: Text('ທຳໃນເບື້ອງຕົ້ນ',
                style: TextStyle(
                    fontWeight: FontWeight.w600,
                    color: Colors.white,
                    letterSpacing: 1)),
          ),
          _navItem(context, Icons.menu_book_sharp, '📚(ຄະຣາວາສຊັ້ນເລີດ)',
              suffix: Text(
                '',
                style: TextStyle(fontWeight: FontWeight.w500),
              ), onTap: () {
            _navItemClicked(context, 1);
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => Book1Lao()),
            );
          }, selected: selectedIndex == 1),
          _navItem(context, Icons.menu_book_sharp, '📚(ຄູ່ມືໂສດາບັນ)',
              suffix: Text(
                '',
                style: TextStyle(fontWeight: FontWeight.w500),
              ), onTap: () {
            _navItemClicked(context, 2);
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => Book2Lao()),
            );
          }, selected: selectedIndex == 2),
          _navItem(context, Icons.menu_book_sharp, '📚(ທານ)',
              suffix: Text(
                '',
                style: TextStyle(fontWeight: FontWeight.w500),
              ), onTap: () {
            _navItemClicked(context, 3);
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => Book3Lao()),
            );
          }, selected: selectedIndex == 3),
          _navItem(context, Icons.menu_book_sharp, '📚(ສາທະຍາຍທັມ)',
              suffix: Text(
                '',
                style: TextStyle(fontWeight: FontWeight.w500),
              ), onTap: () {
            _navItemClicked(context, 4);
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => Book4Lao()),
            );
          }, selected: selectedIndex == 4),
          _navItem(context, Icons.menu_book_sharp, '📚(ພຸດທະວະຈະນະ(ໂດຍພາບລວມ))',
              suffix: Text(
                '',
                style: TextStyle(fontWeight: FontWeight.w500),
              ), onTap: () {
            _navItemClicked(context, 5);
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => Book5Lao()),
            );
          }, selected: selectedIndex == 5),
          Padding(
            padding: EdgeInsets.all(10.10),
            child: Text('ທຳໃນທ່າມກາງ',
                style: TextStyle(
                    fontWeight: FontWeight.w600,
                    color: Colors.white,
                    letterSpacing: 1)),
          ),
          _navItem(context, Icons.menu_book_sharp, '📚(ແກ້ກັມ)',
              suffix: Text(
                '',
                style: TextStyle(fontWeight: FontWeight.w500),
              ), onTap: () {
            _navItemClicked(context, 6);
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => Book6Lao()),
            );
          }, selected: selectedIndex == 6),
          _navItem(context, Icons.menu_book_sharp, '📚(ສະຕິປັດຖານ 4)',
              suffix: Text(
                '',
                style: TextStyle(fontWeight: FontWeight.w500),
              ), onTap: () {
            _navItemClicked(context, 7);
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => Book7Lao()),
            );
          }, selected: selectedIndex == 7),
          Padding(
            padding: EdgeInsets.all(10.10),
            child: Text('ທຳໃນທີສຸດ',
                style: TextStyle(
                    fontWeight: FontWeight.w600,
                    color: Colors.white,
                    letterSpacing: 1)),
          ),
          _navItem(context, Icons.menu_book_sharp, '📚(ອິດທິບາດ 4)',
              suffix: Text(
                '',
                style: TextStyle(fontWeight: FontWeight.w500),
              ), onTap: () {
            _navItemClicked(context, 8);
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => Book8Lao()),
            );
          }, selected: selectedIndex == 8),
          Padding(
            padding: EdgeInsets.all(10.10),
            child: Text('ເເຜນຜັງ',
                style: TextStyle(
                    fontWeight: FontWeight.w600,
                    color: Colors.white,
                    letterSpacing: 1)),
          ),
          _navItem(context, Icons.menu_book_sharp, '📚(ປະຕິຈະສະມຸບາດ)',
              suffix: Text(
                '',
                style: TextStyle(fontWeight: FontWeight.w500),
              ), onTap: () {
            _navItemClicked(context, 9);
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => Diagram1Lao()),
            );
          }, selected: selectedIndex == 9),
        ],
      ),
    );
  }

  _navItem(BuildContext context, IconData icon, String text,
          {Text? suffix, onTap, bool selected = false}) =>
      Container(
        color: selected ? Colors.brown[300] : Colors.transparent,
        child: ListTile(
          leading: Icon(icon,
              color: selected ? Theme.of(context).canvasColor : Colors.white),
          trailing: suffix,
          title: Text(
            text,
            style: TextStyle(
              fontWeight: FontWeight.w500,
              color: Colors.white,
            ),
          ),
          selected: selected,
          onTap: onTap,
        ),
      );

  _navItemClicked(BuildContext context, int index) {
    setIndex(index);
    Navigator.of(context).pop();
  }
}
